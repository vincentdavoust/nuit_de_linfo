<?php

class ControleurAdmin extends Controller
{
	public function alertes()
	{
		if(isset($this->route["params"]["action"])){
			if($this->route["params"]["action"] == 1 && is_numeric($this->route["params"]["id"])){
				$this->view->alertes = ModeleAdmin::valider($this->route["params"]["id"]);
			}
			else if($this->route["params"]["action"] == 2 && is_numeric($this->route["params"]["id"])){
				$this->view->alertes = ModeleAdmin::invalider($this->route["params"]["id"]);
			}
			else if($this->route["params"]["action"] == 3){
				//$this->view->alertes = ModeleAdmin::envoyerCommentaire();
			}
		}
		$this->view->alertes = ModeleAdmin::getAlertes();
		$this->view->display();
	}
}

?>
