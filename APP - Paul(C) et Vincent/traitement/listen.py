##
## listen.py for main in /home/vincent/nuit_info/APP - Paul(C) et Vincent/traitement
## 
## Made by Vincent Davoust
## <vincent.davoust@gmail.com>
## 
## Started on  Thu Dec  3 21:25:10 2015 Vincent Davoust
## Last update Thu Dec  3 22:00:55 2015 Vincent Davoust
##

import socket

class net :

    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def connect(self, host='localhost', port=1234) :
        self.sock.connect((host, port))

    def mysend(self, msg) :
        totalsent = 0
        while totalsent < MSGLEN:
            sent = self.sock.send(msg[totalsent:])
            if sent == 0:
                raise RuntimeError("socket connection broken")
            totalsent = totalsent + sent

    def myreceive(self):
        chunks = []
        bytes_recd = 0
        while bytes_recd < MSGLEN:
            chunk = self.sock.recv(min(MSGLEN - bytes_recd, 2048))
            if chunk == '':
                raise RuntimeError("socket connection broken")
            chunks.append(chunk)
            bytes_recd = bytes_recd + len(chunk)
            return ''.join(chunks)


class Listen :
    def __init__(self):
        self.conn = net(self)

    def get_event(self) :
        self.conn.send("ready")
        return self.conn.receive(self)
