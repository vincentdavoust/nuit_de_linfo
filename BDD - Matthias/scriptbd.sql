
DROP TABLE Logs;
DROP TABLE Membre;
--
DROP TABLE Message_event;
DROP TABLE Keyword_message;
DROP TABLE Source;
DROP TABLE Compte;
DROP TABLE Message;
DROP TABLE Hashtag_event;
DROP TABLE Event;
DROP TABLE Hashtag;
DROP TABLE Keyword;

--

CREATE TABLE Keyword(
	idkeyword SERIAL,
	word VARCHAR NOT NULL,
	priorite INT NOT NULL,
	PRIMARY KEY('idkeyword')
);

CREATE TABLE Hashtag(
	idht SERIAL,
	tag VARCHAR NOT NULL,
	PRIMARY KEY('idht')
);

CREATE TABLE Event(
	idevent SERIAL,
	intitule VARCHAR NOT NULL,
	datestart TIMESTAMP,
	datefin TIMESTAMP,
	PRIMARY KEY('idevent')
);

CREATE TABLE Hashtag_event(
	hashtag BIGINT,
	event BIGINT,
	PRIMARY KEY(hashtag,event),
	FOREIGN KEY(hastag) REFERENCES Hashtag(idht),
	FOREIGN KEY(event) REFERENCES Event(idevent)
);

CREATE TABLE Message(
	idmessage SERIAL,
	contenu FULLTEXT NOT NULL,
	pertinence FLOAT NOT NULL,
	admin INT,
	PRIMARY KEY('idmessage')
);

CREATE TABLE Compte(
	idcompte SERIAL,
	nom VARCHAR NOT NULL,
	volumeflux INT NOT NULL,
	tauxinfosok FLOAT NOT NULL,
	type VARCHAR NOT NULL,
	PRIMARY KEY('idcompte')
);

CREATE TABLE Source(
	idsource SERIAL,
	contenu FULLTEXT NOT NULL,
	source CHAR NOT NULL,
	compte BIGINT NOT NULL,
	datepost TIMESTAMP NOT NULL,
	message BIGINT NOT NULL,
	PRIMARY KEY('idsource'),
	FOREIGN KEY('message') REFERENCES Message(idmessage),
	FOREIGN KEY('compte') REFERENCES Compte(idcompte)
);

CREATE TABLE Keyword_message(
	keyword BIGINT,
	message BIGINT,
	PRIMARY KEY(keyword,message),
	FOREIGN KEY(keyword) REFERENCES Keyword(idkeyword),
	FOREIGN KEY(message) REFERENCES Source(idmessage)
);

CREATE TABLE Message_event(
	message BIGINT,
	event BIGINT,
	PRIMARY KEY(message,event),
	FOREIGN KEY(message) REFERENCES Source(idmessage),
	FOREIGN KEY(event) REFERENCES Event(idevent)
);

--

CREATE TABLE Membre(
	idmembre SERIAL,
	nom VARCHAR,
	prenom VARCHAR,
	zonegeo VARCHAR,
	mail VARCHAR,
	numero VARCHAR,
	niveau CHAR,
	PRIMARY KEY('idmembre')
);

CREATE TABLE Logs(
	idlog SERIAL,
	ip VARCHAR NOT NULL,
	dateconnect TIMESTAMP NOT NULL,
	state CHAR NOT NULL,
	operateur BIGINT,
	PRIMARY KEY('idlog'),
	FOREIGN KEY(operateur) REFERENCES Membre(idmembre)
);

--

DROP TRIGGER IF Exists checkMessage;

--

CREATE TRIGGER checkMessage BEFORE INSERT
ON Message
FOR EACH ROW
BEGIN
	IF(new.pertinence <= 0 OR new.pertinence >=1) THEN
		raise_application_error(-20101, 'ERROR: pertinence must be between 0 and 1');
	END IF;
END;


