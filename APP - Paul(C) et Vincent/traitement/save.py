##
## save.py for main in /home/vincent/nuit_info/APP - Paul(C) et Vincent/traitement
## 
## Made by Vincent Davoust
## <vincent.davoust@gmail.com>
## 
## Started on  Fri Dec  4 01:30:50 2015 Vincent Davoust
## Last update Fri Dec  4 03:03:01 2015 Vincent Davoust
##

import mysql

def save(msg) :
    self.db = mysql.connect(host='localhost', user='', passwd='', db='')
    self.cursor = db.cursor()

    # check if author exists
    id_auteur = -1
    query = "SELECT idcompte FROM Compte WHERE nom="+msg.a
    cursor.execute(query)
    db.commit()
    row = cursor.fetchone()
    id_auteur = row[0]

    if (id_auteur != -1) :
        query = "INSERT INTO Compte (nom, volumeflux, tauxinfosok, type) VALUES ("+msg.a+", 1, 50, "+msg.t+");"
    else :
        query = "UPDATE Compte SET volumeflux=volumeflux+1 WHERE idcompte="+id_auteur

    query += "INSERT INTO Message (contenu, pertinence) VALUES ("+msg.c+", "+msg.pertinence+");"
    cursor.execute(query)
    db.commit()
    row = cursor.fetchone()
    query = "SELECT idmessage FROM Message WHERE contenu="+msg.c+" AND pertinence="+msg.pertinence+");"
    cursor.execute(query)
    db.commit()
    row = cursor.fetchone()
    id_message = row[0]

    query = "INSERT INTO Source (compte, datepost, message) VALUES ("+id_auteur+", "+msg.d+", "+id_message+");"
    if (msg.event) :
        query += "INSERT INTO Message_event (message, event) VALUES ("+id_message+", "+msg.event+");"
    cursor.execute(query)
    db.commit()
    row = cursor.fetchone()


    # execute SQL select statement
    cursor.execute(query)

    # commit your changes
    db.commit()
