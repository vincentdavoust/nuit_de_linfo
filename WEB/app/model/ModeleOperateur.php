<?php

class ModeleOperateur extends Model
{
	public static function getAlertes(){
		$params = parent::getparams();
		$alerts = array();
		$link = mysqli_connect($params['host'],$params['username'],$params['mdp'],$params['db']);
		if(!$link){
			die('Connexion error');
		}
		$link->set_charset("utf8");
		$res = mysqli_query($link,"SELECT id,contenu,pertinence FROM Message WHERE pertinence<>1 ORDER BY pertinence DESC LIMIT 10;"); 

		for($i=0; $assoc=mysqli_fetch_assoc($res); $i++){
			$alerts[$i] = $assoc;
		}

		return $alerts;
	}
	
	//"UPDATE message SET pertinence=... where ...=idmessage"
}
?>