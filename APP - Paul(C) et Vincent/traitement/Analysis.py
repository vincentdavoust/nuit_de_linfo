##
## Analysis.py for main in /home/vincent/nuit_info/APP - Paul(C) et Vincent/traitement
## 
## Made by Vincent Davoust
## <vincent.davoust@gmail.com>
## 
## Started on  Thu Dec  3 21:25:10 2015 Vincent Davoust
## Last update Fri Dec  4 06:45:37 2015 Vincent Davoust
##

import sys
import mysql.connector
sys.path.append("../commun")
from message import Message


class Analysis :


    # ok
    def __init__(self) :
        conn = mysql.connector.connect(host="localhost",user="root",password="", database="Cookies")
        self.cursor = conn.cursor()

    # should be ok
    def count(self, msg) :
        # querry to count number of occurences where match is close
        conn = mysql.connector.connect(host="localhost",user="root",password="", database="Cookies")
        cursor = conn.cursor()
        query = "SELECT COUNT(*) AS nb\
                        FROM Message \
                        WHERE MATCH (Message.contenu) \
                        AGAINST (""+msg.c+"") > " + '5'

        # execute SQL select statement
        cursor.execute(query)

        # commit your changes
        db.commit()

        row = cursor.fetchone()
        msg.setCount(row[0])

    # not ok
    def grade(self, msg) :
        query = "SELECT (MATCH (words) \
                        AGAINST ('"+msg.c+"' \
                        IN NATURAL LANGUAGE MODE)) * (SELECT taux_info_ok FROM Compte WHERE nom='"+msg.a+"') * \
                        (SELECT MATCH() AGAINST ('"+msg.c+"' IN NATURAL LANGUAGE MODE)) AS grade \
                        FROM Keywords"

        # execute SQL select statement
        cursor.execute(query)

        # commit your changes
        db.commit()

        row = cursor.fetchone()
        msg.setGrade(row[0])

    # not ok, fiab formula to be corrected
    def getFiab(self, msg) :
        query = "SELECT tauxinfosok * volumeflux \
                 FROM Compte \
                 WHERE nom="+msg.a

        # execute SQL select statement
        cursor.execute(query)

        # commit your changes
        db.commit()

        row = cursor.fetchone()
        msg.setFiab(row[0])

    # should be ok
    def getEvent(self, msg) :
        query = "SELECT idevent \
                 FROM Message, Message_event \
                 WHERE Message.idmessage = Message_event.idmessage \
                 ORDER BY MATCH (contenu) \
                 AGAINST ('"+msg.c+"' IN NATURAL LANGUAGE MODE) \
                 LIMIT 1,1"

        # execute SQL select statement
        cursor.execute(query)

        # commit your changes
        db.commit()

        row = cursor.fetchone()
        msg.setEvent(row[0])
