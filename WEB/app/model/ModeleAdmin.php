<?php

class ModeleAdmin extends Model
{
	public static function valider($id){
		$params = parent::getparams();

		$link = mysqli_connect($params['host'],$params['username'],$params['mdp'],$params['db']);
		if(!$link){
			die('Connexion error');
		}

		$link->set_charset("utf8");

		mysqli_query($link,"UPDATE message SET admin=1 where idmessage=".$id);
	}

	public static function invalider($id){
		$params = parent::getparams();

		$link = mysqli_connect($params['host'],$params['username'],$params['mdp'],$params['db']);
		if(!$link){
			die('Connexion error');
		}

		$link->set_charset("utf8");

		mysqli_query($link,"UPDATE message SET admin=0 where idmessage=".$id);
	}

	public static function getAlertes(){
		$params = parent::getparams();
		$alerts = array();
		$link = mysqli_connect($params['host'],$params['username'],$params['mdp'],$params['db']);
		if(!$link){
			die('Connexion error');
		}
		$link->set_charset("utf8");
		$res = mysqli_query($link,"SELECT idmessage, contenu FROM message WHERE pertinence=1 LIMIT 10;"); 

		for($i=0; $assoc=mysqli_fetch_assoc($res); $i++){
			$alerts[$i] = $assoc;
		}

		return $alerts;
	}
	//"UPDATE message SET pertinence=... where ...=idmessage"
}
?>