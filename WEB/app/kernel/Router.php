<?php
class Router
{
	public static function analyze($query)
	{
		$result = array(
			"controller" => "Error",
			"action" => "error404",
			"params" => array()
		);

		if( $query === "" || $query === "/" )
		{
			$result["controller"] = "Accueil";
			$result["action"] = "accueil";
		}
		else
		{
			$parts = explode("/", $query);

			if ($parts[0] == "accueil")
			{
				$result["controller"] = "Accueil";
				$result["action"] = "accueil";
			}
			else if($parts[0] == "operateur"){
				$result["controller"] = "Operateur";
				if(isset($parts[1])){
					if($parts[1]== "alertes"){
						$result["action"] = "alertes";
					}
					else{
					
					}
				}
				else{
					
				}
			}
			else if($parts[0] == "admin"){
				$result["controller"] = "Admin";
				if(isset($parts[1])){
					if($parts[1] == "alertes"){	
						$result["action"] = "alertes";
						if(count($parts) == 3){
							$result["params"]["action"] = $parts[2];
						}
						if(count($parts) == 4){
							$result["params"]["id"] = $parts[3];
							$result["params"]["action"] = $parts[2];
						}
					}
					else{
					
					}
				}
				else{

				}
			}
		}

		return $result;
	}
}